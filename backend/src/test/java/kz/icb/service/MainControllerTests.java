package kz.icb.service;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import kz.icb.service.ScheduleService.ScheduleResponse;

@SpringBootTest
@AutoConfigureMockMvc
class MainControllerTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Test
	public void getSchedules() throws Exception {
		ScheduleResponse resp = new ScheduleResponse();
		resp.scheduleId = 1L;
		resp.routeName = "Almaty-Shymkent";
		resp.boardingTime = LocalDateTime.parse("2020-06-08T10:00:00");
		resp.departureTime = LocalDateTime.parse("2020-06-08T12:00:00");
		resp.arrivalTime = LocalDateTime.parse("2020-06-09T08:00:00");
		resp.fleetPlateNumber = "123ABC02";
		resp.fleetBrand = "Daewoo";
		resp.fleetModel = "BH120F";
		resp.fleetCapacity = 49;
		resp.price = 5000.0;
		
		List<ScheduleResponse> list = new ArrayList<>();
		list.add(resp);
		
		
		String json = objectMapper.writeValueAsString(list);
		System.out.println(json);

		mockMvc.perform(get("/schedule/2020-06-08/4")).andExpect(status().isOk()).andExpect(content().json(json));
	}

}
