package kz.icb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import kz.icb.model.BeanMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Service
@RequiredArgsConstructor
public class ScheduleService {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	private BeanMapper beanMapper;
	
	public List<ScheduleResponse> getSchedules(LocalDate departureDate, Long departureStationId, Long destinationStationId) {
		String sql = 
				"select " +
				"	sch.id, " +
				"	r.name as route_name, " +
				"	sch.boarding_time, " +
				"	sch.departure_time, " +
				"	sch.arrival_time, " +
				"	f.plate_number, " + 
				"	f.brand, " + 
				"	f.model, " + 
				"	f.capacity, " + 
				"	sch.price " + 
				"from" +
				"	icb.icb_route r," + 
				"	icb.icb_route_station r_dept," + 
				"	icb.icb_route_station r_dest, " + 
				"   icb.icb_schedule sch," + 
				"   icb.icb_fleet f " + 
				"where" + 
				"   r.id = r_dept.route_id" + 
				"   and r.id = sch.route_id" + 
				"   and f.id = sch.fleet_id" + 
				"   and r.id = r_dest.route_id" + 
				"   and r.id = r_dept.route_id" + 
				"   and r_dest.order_seq > r_dept.order_seq" + 

				"   and r_dept.station_id = :departureStationId" + 
				"   and r_dest.station_id = :destinationStationId" + 
				"   and date(sch.departure_time) = :departureDate"; 

		Map<String, Object> params = new HashMap<>();
		params.put("departureStationId", departureStationId);
		params.put("destinationStationId", destinationStationId);
		params.put("departureDate", departureDate);
		
		
		return jdbcTemplate.queryForList(sql, params).stream()
				.map(beanMapper::dbObjectToScheduleResponse)
				.collect(Collectors.toList());
	}
	
	public List<ScheduleResponse> getSchedules(LocalDate departureDate, Long departureStationId) {
		String sql = 
				"select " +
				"	sch.id, " +
				"	r.name as route_name, " +
				"	sch.boarding_time, " +
				"	sch.departure_time, " +
				"	sch.arrival_time, " +
				"	f.plate_number, " + 
				"	f.brand, " + 
				"	f.model, " + 
				"	f.capacity, " + 
				"	sch.price " + 
				"from" +
				"	icb.icb_route r," + 
				"	icb.icb_route_station r_dept," + 
				"   icb.icb_schedule sch," + 
				"   icb.icb_fleet f " + 
				"where" + 
				"   r.id = r_dept.route_id" + 
				"   and r.id = sch.route_id" + 
				"   and f.id = sch.fleet_id" + 
				"   and r.id = r_dept.route_id" + 

				"   and r_dept.station_id = :departureStationId" + 
				"   and date(sch.departure_time) = :departureDate"; 

		Map<String, Object> params = new HashMap<>();
		params.put("departureStationId", departureStationId);
		params.put("departureDate", departureDate);
		
		
		return jdbcTemplate.queryForList(sql, params).stream()
				.map(beanMapper::dbObjectToScheduleResponse)
				.collect(Collectors.toList());
	}
	
	public OccupiedPlacesResponse getOccupiedPlaces(Long scheduleId) {
		String sql = 
				"select " +
				"	place_number " +
				"from" +
				"   icb.icb_order o," + 
				"   icb.icb_order_place p " + 
				"where" + 
				"   o.schedule_id = :scheduleId" + 
				"   and o.id = p.order_id" +
				"   and o.state = 'active'"; 

		Map<String, Object> params = new HashMap<>();
		params.put("scheduleId", scheduleId);
		
		List<String> places = jdbcTemplate.queryForList(sql, params).stream()
				.map(place -> place.get("place_number").toString())
				.collect(Collectors.toList());
		
		return new OccupiedPlacesResponse(places.size(), places);
	}
	
	@Getter
	@Setter
	public static class ScheduleResponse {
		public Long scheduleId;
		public String routeName;
		public LocalDateTime boardingTime;
		public LocalDateTime departureTime;
		public LocalDateTime arrivalTime;
		public String fleetPlateNumber;
		public String fleetBrand;
		public String fleetModel;
		public Integer fleetCapacity;
		public Double price;
	}
	
	@AllArgsConstructor
	@Getter
	@Setter
	public static class OccupiedPlacesResponse {
		public Integer count;
		public List<String> places;
	}
	
}
