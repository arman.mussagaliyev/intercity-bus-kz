package kz.icb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import kz.icb.model.BeanMapper;
import kz.icb.model.Station;
import lombok.RequiredArgsConstructor;

@Service
@Repository
@RequiredArgsConstructor
public class StationService {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	private BeanMapper beanMapper;
	
	public List<Station> getDepartureStations() {
		Map<String, ?> params = new HashMap<>();
		return jdbcTemplate.queryForList("select * from icb.icb_station order by name", params)
				.stream().map(beanMapper::dbObjectToStation)
				.collect(Collectors.toList());
	}
	
	public List<Station> getDestinationStations(Long departureStationId) {
		
		String sql = "select * " + 
				"from icb.icb_station dest " + 
				"where exists(select 1" + 
				"             from icb.icb_route r," + 
				"                  icb.icb_route_station r_dept," + 
				"                  icb.icb_route_station r_dest " + 
				"             where" + 
				"                   r.id = r_dept.route_id" + 
				"                   and r.id = r_dest.route_id" + 
				"                   and r_dest.station_id = dest.id" + 
				"                   and r_dest.order_seq > r_dept.order_seq" + 
				"                   and r_dept.station_id = :departureStationId)"; 

		Map<String, Object> params = new HashMap<>();
		params.put("departureStationId", departureStationId);
		
		return jdbcTemplate.queryForList(sql, params).stream()
				.map(beanMapper::dbObjectToStation)
				.collect(Collectors.toList());
	}
	
}
