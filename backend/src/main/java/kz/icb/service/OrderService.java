package kz.icb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kz.icb.model.Order;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final NamedParameterJdbcTemplate jdbcTemplate;
	private final SmsService smsService;

	@Transactional
	public OrderCreationResponse createOrder(OrderCreationRequest request) {
		String sql = "insert into icb.icb_order (schedule_id, client_phone, confirmation_code, state) values (:scheduleId, :clientPhone, '000000', 'created')";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource params = new BeanPropertySqlParameterSource(request);
		jdbcTemplate.update(sql, params, keyHolder);
		
		String confirmationCode = smsService.sendCode(request.getClientPhone());
		
		setOrderConfirmationCode(keyHolder.getKey().longValue(), confirmationCode);
		
		createOrderPlaces(keyHolder.getKey().longValue(), request.placeNumbers);
		
		return new OrderCreationResponse(keyHolder.getKey().longValue());
	}
	
	@Transactional
	public OrderConfirmationResponse confirmOrder(OrderConfirmationRequest request) {
		Order order = getOrder(request.getOrderId());
		if (order.getState().equals("created")) {
			if (order.getConfirmationCode().equals(request.getConfirmationCode())) {
				setOrderState(request.getOrderId(), "active");
				return new OrderConfirmationResponse("ORDER_CONFIRMED");
			} else {
				return new OrderConfirmationResponse("ORDER_CONFIRMATION_CODE_DOES_NOT_MATCH");
			}
		} else if (order.getState().equals("active")) {
			return new OrderConfirmationResponse("ORDER_ALREADY_CONFIRMED");
		} else if (order.getState().equals("cancelled")) {
			return new OrderConfirmationResponse("ORDER_CANCELLED");
		}
		return new OrderConfirmationResponse("ORDER_NOT_CONFIRMED");
	}
	
	@Transactional
	public void cancelOrder(Long orderId) {
		setOrderState(orderId, "cancelled");
	}
	
	private void createOrderPlaces(Long orderId, List<String> places) {
		String sql = "insert into icb.icb_order_place (order_id, place_number) values (:orderId, :placeNumber)";
		List<Map<String, Object>> params = places.stream().map(placeNumber -> {
			Map<String, Object> rowParams = new HashMap<>();
			rowParams.put("orderId", orderId);
			rowParams.put("placeNumber", placeNumber);
			return rowParams;
		}).collect(Collectors.toList());
		
		Map<String, Object>[] paramsArray = new Map[params.size()];
		jdbcTemplate.batchUpdate(sql, params.toArray(paramsArray));
	}
	
	private void setOrderConfirmationCode(Long orderId, String confirmationCode) {
		String sql = "update icb.icb_order set confirmation_code = :confirmationCode where id = :orderId";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("orderId", orderId);
		params.addValue("confirmationCode", confirmationCode);
		jdbcTemplate.update(sql, params);
	}
	
	private void setOrderState(Long orderId, String state) {
		String sql = "update icb.icb_order set state = :state where id = :orderId";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("orderId", orderId);
		params.addValue("state", state);
		jdbcTemplate.update(sql, params);
	}
	
	private Order getOrder(Long orderId) {
		String sql = "select * from icb.icb_order where id = :orderId";
		Map<String, Object> params = new HashMap<>();
		params.put("orderId", orderId);
		return jdbcTemplate.queryForObject(sql, params, (rs, num) -> {
			return new Order()
					.setId(rs.getLong("id"))
					.setScheduleId(rs.getLong("schedule_id"))
					.setClientPhone(rs.getString("client_phone"))
					.setConfirmationCode(rs.getString("confirmation_code"))
					.setState(rs.getString("state"));
			
		});
	}
	
	@Getter
	@Setter
	@Accessors(chain=true)
	public static class OrderCreationRequest {
		public Long scheduleId;
		public String clientPhone;
		public List<String> placeNumbers;
	}

	@Getter
	@Setter
	@Accessors(chain=true)
	@AllArgsConstructor
	public static class OrderCreationResponse {
		public Long orderId;
	}

	@Getter
	@Setter
	@Accessors(chain=true)
	public static class OrderConfirmationRequest {
		public Long orderId;
		public String confirmationCode;
	}

	@Getter
	@Setter
	@Accessors(chain=true)
	@AllArgsConstructor
	public static class OrderConfirmationResponse {
		public String resultCode;
	}
}
