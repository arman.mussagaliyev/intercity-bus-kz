package kz.icb.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import kz.icb.model.Station;
import kz.icb.service.OrderService;
import kz.icb.service.OrderService.OrderConfirmationRequest;
import kz.icb.service.OrderService.OrderConfirmationResponse;
import kz.icb.service.OrderService.OrderCreationRequest;
import kz.icb.service.OrderService.OrderCreationResponse;
import kz.icb.service.ScheduleService;
import kz.icb.service.ScheduleService.OccupiedPlacesResponse;
import kz.icb.service.ScheduleService.ScheduleResponse;
import kz.icb.service.StationService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class MainController {
	
	private final StationService stationService;
	private final ScheduleService scheduleService;
	private final OrderService orderService;
	
	@GetMapping(path="/station/departure", produces="application/json")
	public List<Station> getDepartureStations() {
		return stationService.getDepartureStations();
	}

	@GetMapping("/station/destination")
	public List<Station> getDestinationStations(@RequestParam Long departureStationId) {
		return stationService.getDestinationStations(departureStationId);
	}

	@GetMapping("/schedule/{departureDate}/{departureStationId}/{destinationStationId}")
	public List<ScheduleResponse> getSchedule(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate departureDate, @PathVariable Long departureStationId,
			@PathVariable Long destinationStationId) {
		return scheduleService.getSchedules(departureDate, departureStationId, destinationStationId);
	}

	@GetMapping("/schedule/{departureDate}/{departureStationId}")
	public List<ScheduleResponse> getSchedule(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate departureDate, @PathVariable Long departureStationId) {
		return scheduleService.getSchedules(departureDate, departureStationId);
	}

	@PostMapping("/order")
	public OrderCreationResponse createOrder(@RequestBody OrderCreationRequest orderCreationRequest) {
		return orderService.createOrder(orderCreationRequest);
	}
	
	@GetMapping("/schedule/{scheduleId}/places")
	public OccupiedPlacesResponse getOccupiedPlaces(@PathVariable Long scheduleId) {
		return scheduleService.getOccupiedPlaces(scheduleId);
	}

	@PostMapping("/order/confirm")
	public OrderConfirmationResponse confirmOrder(@RequestBody OrderConfirmationRequest orderConfirmationRequest) {
		return orderService.confirmOrder(orderConfirmationRequest);
	}
	
	@PostMapping("/order/{orderId}/cancel")
	@ResponseStatus(value = HttpStatus.OK)
	public void cancelOrder(@PathVariable Long orderId) {
		orderService.cancelOrder(orderId);
	}
}
