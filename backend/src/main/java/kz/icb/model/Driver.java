package kz.icb.model;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Driver {
	public Long id;
	public LocalDateTime created;
	public String name;
	public String email;
	public String phone;
	public String state;
}
