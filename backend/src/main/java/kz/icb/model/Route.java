package kz.icb.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Route {
	
	public Long id;
	public String name;
	public List<Station> stations;

}
