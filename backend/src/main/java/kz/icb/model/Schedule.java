package kz.icb.model;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Schedule {
	public Long id;
	public Fleet fleet;
	public Driver driver;
	public Route route;
	public LocalDateTime boardingTime;
	public LocalDateTime departureTime;
	public LocalDateTime arrivalTime;
}
