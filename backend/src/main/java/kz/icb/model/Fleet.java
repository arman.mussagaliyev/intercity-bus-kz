package kz.icb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Fleet {
	public Long id;
	public String plateNumber;
	public Integer capacity;
	public String brand;
	public String model;
	public String state;
}
