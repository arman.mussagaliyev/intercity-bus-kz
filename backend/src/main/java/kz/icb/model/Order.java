package kz.icb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Order {
	public Long id;
	public Long scheduleId;
	public String clientPhone;
	public String confirmationCode;
	public String state;
}
