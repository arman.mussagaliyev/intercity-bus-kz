package kz.icb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Station {
	
	public Long id;
	public String name;
	
}
