package kz.icb.model;

import java.math.BigDecimal;
import java.util.Map;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import kz.icb.service.ScheduleService.ScheduleResponse;

@Mapper(componentModel = "spring")
public interface BeanMapper {
    @Mapping(expression = "java(Long.valueOf(dbObject.get(\"id\").toString()))", target = "id")
	@Mapping(expression = "java(dbObject.get(\"name\").toString())", target = "name")
    Station dbObjectToStation(Map<String, Object> dbObject);

    @Mapping(expression = "java(Long.valueOf(dbObject.get(\"id\").toString()))", target = "id")
    @Mapping(expression = "java(((java.sql.Timestamp)dbObject.get(\"boarding_time\")).toLocalDateTime())", target = "boardingTime")
    @Mapping(expression = "java(((java.sql.Timestamp)dbObject.get(\"departure_time\")).toLocalDateTime())", target = "departureTime")
    @Mapping(expression = "java(((java.sql.Timestamp)dbObject.get(\"arrival_time\")).toLocalDateTime())", target = "arrivalTime")
	@Mapping(expression = "java(map.get(\"route_name\").toString())", target = "route.name")
	@Mapping(expression = "java(map.get(\"plate_number\").toString())", target = "fleet.plateNumber")
	@Mapping(expression = "java(Integer.valueOf(map.get(\"capacity\").toString()))", target = "fleet.capacity")
	@Mapping(expression = "java(map.get(\"brand\").toString())", target = "fleet.brand")
	@Mapping(expression = "java(map.get(\"model\").toString())", target = "fleet.model")
    Schedule dbObjectToSchedule(Map<String, Object> dbObject);
    
    @Mapping(expression = "java(Long.valueOf(dbObject.get(\"id\").toString()))", target = "scheduleId")
    @Mapping(expression = "java(((java.sql.Timestamp)dbObject.get(\"boarding_time\")).toLocalDateTime())", target = "boardingTime")
    @Mapping(expression = "java(((java.sql.Timestamp)dbObject.get(\"departure_time\")).toLocalDateTime())", target = "departureTime")
    @Mapping(expression = "java(((java.sql.Timestamp)dbObject.get(\"arrival_time\")).toLocalDateTime())", target = "arrivalTime")
	@Mapping(expression = "java(dbObject.get(\"route_name\").toString())", target = "routeName")
	@Mapping(expression = "java(dbObject.get(\"plate_number\").toString())", target = "fleetPlateNumber")
	@Mapping(expression = "java(Integer.valueOf(dbObject.get(\"capacity\").toString()))", target = "fleetCapacity")
	@Mapping(expression = "java(dbObject.get(\"brand\").toString())", target = "fleetBrand")
	@Mapping(expression = "java(dbObject.get(\"model\").toString())", target = "fleetModel")
	@Mapping(source="dbObject", target = "price", qualifiedByName="extractPrice")
    ScheduleResponse dbObjectToScheduleResponse(Map<String, Object> dbObject);

    @Named("extractPrice")
    default Double extractPrice(Map<String, Object> dbObject) {
    	return ((BigDecimal)dbObject.get("price")).doubleValue();
    }

	//@Mapping(expression = "java(Double.valueOf(dbObject.get(\"price\").toString()))", target = "price")
}
