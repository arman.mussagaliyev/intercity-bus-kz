--liquibase formatted sql

--changeset arman:stations_test_data context:prod

insert into icb.icb_station (name) values ('Aksu');
insert into icb.icb_station (name) values ('Karasu');
insert into icb.icb_station (name) values ('Enbek');
insert into icb.icb_station (name) values ('Almaty');
insert into icb.icb_station (name) values ('Shymkent');
insert into icb.icb_station (name) values ('Taldykorgan');
insert into icb.icb_station (name) values ('Nursultan');
insert into icb.icb_station (name) values ('Issyk');
insert into icb.icb_station (name) values ('Karagandy');

--rollback delete from icb.icb_station
