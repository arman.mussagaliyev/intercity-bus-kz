--liquibase formatted sql

--changeset arman:routes_test_data context:"test or local"

insert into icb.icb_route (name, state) values ('Almaty-Shymkent', 'active');
insert into icb.icb_route (name, state) values ('Almaty-Karagandy', 'active');

--rollback delete from icb.icb_station
