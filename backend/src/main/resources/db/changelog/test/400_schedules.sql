--liquibase formatted sql

--changeset arman:schedules_test_data context:"test or local"

insert into icb.icb_fleet(plate_number, capacity, brand, model, state) values ('ABA123', 20, 'Toyota', 'Model S', 'active');
insert into icb.icb_fleet(plate_number, capacity, brand, model, state) values ('ABA124', 20, 'Toyota', 'Model S', 'active');
insert into icb.icb_fleet(plate_number, capacity, brand, model, state) values ('123ABC02', 49, 'Daewoo', 'BH120F', 'active');
insert into icb.icb_fleet(plate_number, capacity, brand, model, state) values ('456DEF05', 53, 'Volvo', '9500', 'active');


insert into icb.icb_driver(name, email, phone, state) values ('Name1', 'email1', 'phone1', 'active');
insert into icb.icb_driver(name, email, phone, state) values ('Name2', 'email2', 'phone2', 'active');
insert into icb.icb_driver(name, email, phone, state) values ('ISABEKOV BOLAT', 'ibolat15@mail.ru', '87770123456', 'active');
insert into icb.icb_driver(name, email, phone, state) values ('AIDARULY KAIRAT', 'aikai17@gmail.com', '87770654987', 'active');


insert into icb.icb_schedule (fleet_id, driver_id, route_id, boarding_time, departure_time, arrival_time, price) values (3, 3, 1, '2020-06-08 10:00:00', '2020-06-08 12:00:00', '2020-06-09 08:00:00', 5000);
insert into icb.icb_schedule (fleet_id, driver_id, route_id, boarding_time, departure_time, arrival_time, price) values (4, 4, 2, '2020-06-08 10:00:00', '2020-06-08 12:00:00', '2020-06-09 08:00:00', 5000);

--rollback delete from icb.icb_route_station
