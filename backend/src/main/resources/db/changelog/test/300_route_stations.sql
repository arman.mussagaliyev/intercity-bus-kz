--liquibase formatted sql

--changeset arman:route_stations_test_data context:"test or local"

insert into icb.icb_route_station (route_id, station_id, order_seq) values (1, 4, 1);
insert into icb.icb_route_station (route_id, station_id, order_seq) values (1, 5, 5);
insert into icb.icb_route_station (route_id, station_id, order_seq) values (1, 1, 2);
insert into icb.icb_route_station (route_id, station_id, order_seq) values (1, 2, 3);
insert into icb.icb_route_station (route_id, station_id, order_seq) values (1, 3, 4);

--rollback delete from icb.icb_route_station
