--liquibase formatted sql

--changeset arman:orders_test_data context:"test or local"

insert into icb.icb_order (id, schedule_id, client_phone, confirmation_code, state) values (1, 1, '+777789765432', '000001', 'active');
insert into icb.icb_order (id, schedule_id, client_phone, confirmation_code, state) values (2, 1, '+777789765432', '000002', 'cancelled');
insert into icb.icb_order (id, schedule_id, client_phone, confirmation_code, state) values (3, 1, '+777789765434', '000003', 'active');
insert into icb.icb_order (id, schedule_id, client_phone, confirmation_code, state) values (4, 2, '+777789765434', '000004', 'active');

--rollback delete from icb.icb_route_station

--changeset arman:order_places_test_data context:test

insert into icb.icb_order_place (order_id, place_number) values (1, '1A');
insert into icb.icb_order_place (order_id, place_number) values (1, '1B');
insert into icb.icb_order_place (order_id, place_number) values (1, '1C');
insert into icb.icb_order_place (order_id, place_number) values (1, '1D');
insert into icb.icb_order_place (order_id, place_number) values (1, '2A');
insert into icb.icb_order_place (order_id, place_number) values (1, '3A');
insert into icb.icb_order_place (order_id, place_number) values (1, '4A');
insert into icb.icb_order_place (order_id, place_number) values (1, '5A');

insert into icb.icb_order_place (order_id, place_number) values (2, '12A');
insert into icb.icb_order_place (order_id, place_number) values (2, '12B');

insert into icb.icb_order_place (order_id, place_number) values (3, '13A');
insert into icb.icb_order_place (order_id, place_number) values (3, '13B');

insert into icb.icb_order_place (order_id, place_number) values (4, '14A');
insert into icb.icb_order_place (order_id, place_number) values (4, '14B');

--rollback delete from icb.icb_route_station
