--liquibase formatted sql

--changeset arman:create_tables logicalFilePath:create_tables

CREATE SCHEMA IF NOT EXISTS `icb`;

CREATE TABLE IF NOT EXISTS `icb`.`icb_driver` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(20) NOT NULL,
  `state` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_fleet` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `plate_number` VARCHAR(45) NOT NULL,
  `capacity` INT(11) NULL DEFAULT NULL,
  `brand` VARCHAR(45) NOT NULL,
  `model` VARCHAR(45) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `plate_number_UNIQUE` (`plate_number` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_route` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_station` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_route_station` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `route_id` INT(11) NOT NULL,
  `station_id` INT(11) NOT NULL,
  `order_seq` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_icb_route_stop_icb_route_idx` (`route_id` ASC),
  INDEX `fk_icb_route_stop_icb_stop1_idx` (`station_id` ASC),
  CONSTRAINT `fk_icb_route_stop_icb_route`
    FOREIGN KEY (`route_id`)
    REFERENCES `icb`.`icb_route` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_icb_route_stop_icb_stop1`
    FOREIGN KEY (`station_id`)
    REFERENCES `icb`.`icb_station` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_schedule` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `fleet_id` INT(11) NOT NULL,
  `driver_id` INT(11) NOT NULL,
  `route_id` INT(11) NOT NULL,
  `boarding_time` DATETIME NOT NULL,
  `departure_time` DATETIME NOT NULL,
  `arrival_time` DATETIME NOT NULL,
  `price` DECIMAL NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_icb_schedule_icb_bus1_idx` (`fleet_id` ASC),
  INDEX `fk_icb_schedule_icb_driver1_idx` (`driver_id` ASC),
  INDEX `fk_icb_schedule_icb_route1_idx` (`route_id` ASC),
  CONSTRAINT `fk_icb_schedule_icb_bus1`
    FOREIGN KEY (`fleet_id`)
    REFERENCES `icb`.`icb_fleet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_icb_schedule_icb_driver1`
    FOREIGN KEY (`driver_id`)
    REFERENCES `icb`.`icb_driver` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_icb_schedule_icb_route1`
    FOREIGN KEY (`route_id`)
    REFERENCES `icb`.`icb_route` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` INT(11) NOT NULL,
  `client_phone` VARCHAR(45) NOT NULL,
  `confirmation_code` VARCHAR(10) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_icb_order_icb_schedule1_idx` (`schedule_id` ASC),
  CONSTRAINT `fk_icb_order_icb_schedule1`
    FOREIGN KEY (`schedule_id`)
    REFERENCES `icb`.`icb_schedule` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `icb`.`icb_order_place` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_id` INT(11) NOT NULL,
  `place_number` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_icb_order_place_icb_order1_idx` (`order_id` ASC),
  CONSTRAINT `fk_icb_order_place_icb_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `icb`.`icb_order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

--rollback <rollback SQL statements>
