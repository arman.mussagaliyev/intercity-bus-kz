# Intercity Bus Backend Application

To run application locally execute following command

For Linux
```
cd backend
./mvnw spring-boot:run -Dspring-boot.run.profiles=test
```

For Windows
```
cd backend
mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=test
```

and then try following URL in your browser

```
http://localhost:8080/station/departure
```

one of the last logs should be

```
2020-06-08 21:48:38.034  INFO 1456 --- [  restartedMain] liquibase.executor.jvm.JdbcExecutor      : insert into icb.icb_station (name) values ('Aksu')
2020-06-08 21:48:38.035  INFO 1456 --- [  restartedMain] liquibase.executor.jvm.JdbcExecutor      : insert into icb.icb_station (name) values ('Karasu')
2020-06-08 21:48:38.036  INFO 1456 --- [  restartedMain] liquibase.executor.jvm.JdbcExecutor      : insert into icb.icb_station (name) values ('Enbek')
```

to connect to DB use following URL

```
http://localhost:8080/h2-console
```

and press connect