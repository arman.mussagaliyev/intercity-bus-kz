# Intercity Bus KZ

Marketplace for intercity buses and passengers in Kazakhstan
Frontend branch

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to run this project locally:

1. After cloning the repository - enter into the frontend directory:
   ### `cd frontend`
2. Run command:
   ### `yarn install` (or npm install)
   - this will install all dependencies for a project
3. After installation, run command:
   ### `yarn start` (or npm start)
   Runs the app in the development mode.<br />
   Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
   The page will reload if you make edits.<br />
   You will also see any lint errors in the console.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
