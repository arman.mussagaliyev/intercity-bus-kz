import backend from '../apis/backend';
import {
  FETCH_DEP,
  FETCH_DEP_SUCCESS,
  FETCH_DEP_ERROR,
  SELECT_DEP,
  FETCH_ARRIVALS,
  FETCH_ARRIVALS_SUCCESS,
  FETCH_ARRIVALS_ERROR,
  SELECT_ARRIVAL,
  SELECT_DEP_DATE,
  FETCH_SCHEDULES,
  FETCH_SCHEDULES_ERROR,
  FETCH_SCHEDULES_SUCCESS
} from './types';

export const fetchDepartures = () => async dispatch => {
  dispatch({
    type: FETCH_DEP,
    payload: 'Загрузка списка...'
  });
  try {
    const responce = await backend.get('/departure');
    dispatch({
      type: FETCH_DEP_SUCCESS,
      payload: responce.data
    });
  } catch (error) {
    dispatch({
      type: FETCH_DEP_ERROR,
      payload: error.response.data.error
    });
  }
};

export const fetchArrivals = depId => async dispatch => {
  dispatch({
    type: FETCH_ARRIVALS,
    payload: 'Загрузка списка...'
  });
  try {
    const responce = await backend.get(
      `/destination?departureStationId=${depId}`
    );
    dispatch({
      type: FETCH_ARRIVALS_SUCCESS,
      payload: responce.data
    });
  } catch (error) {
    dispatch({
      type: FETCH_ARRIVALS_ERROR,
      payload: error.response.data.error
    });
  }
};

export const selectDepAndFetchArrivals = depId => async dispatch => {
  await dispatch(fetchArrivals(depId));
  dispatch(selectDeparture(depId));
};

export const selectDeparture = depId => dispatch => {
  dispatch({
    type: SELECT_DEP,
    payload: depId
  });
};

export const selectArrival = arrivalId => dispatch => {
  dispatch({
    type: SELECT_ARRIVAL,
    payload: arrivalId
  });
};

export const selectDepDate = depDate => dispatch => {
  dispatch({
    type: SELECT_DEP_DATE,
    payload: depDate
  });
};

export const fetchSchedules = (depDate, depId, arrivalId) => async dispatch => {
  dispatch({
    type: FETCH_SCHEDULES,
    payload: 'Загрузка рейсов...'
  });
  try {
    const responce = await backend.get(
      `/schedule/${depDate}/${depId}/${arrivalId}`
    );
    dispatch({
      type: FETCH_SCHEDULES_SUCCESS,
      payload: responce.data
    });
  } catch (error) {
    dispatch({
      type: FETCH_SCHEDULES_ERROR,
      payload: [
        'Не указана станция отправления/прибытия.',
        '(возможно, сбой подключения к Интернету)'
      ]
    });
  }
};
