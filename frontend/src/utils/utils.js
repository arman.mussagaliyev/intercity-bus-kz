export function savePhoneToLocalStore(phone) {
  localStorage.setItem('phone', JSON.stringify(phone));
}

export function getPhoneFromLS() {
  if (typeof localStorage.phone !== 'undefined') {
    return JSON.parse(localStorage.phone);
  }
  return null;
}

export function getCurrDateString() {
  let local = new Date();
  return local.toJSON().slice(0, 10);
}
