import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Container, Header, Input, Button } from 'semantic-ui-react';

import './Login.css';

function Login({ registerPhoneNumber, changePhoneNumber, btnRegDisabled }) {
  useEffect(() => {
    document.querySelector('.regPhone').disabled = btnRegDisabled;
  });
  return (
    <Container className="login">
      <Header as="h3">
        Введите Ваш номер телефона <br /> для входа:
      </Header>

      <Input
        label="+7 "
        focus
        type="tel"
        placeholder="Номер Вашего телефона"
        pattern="^[0-9]+$"
        minLength={10}
        maxLength={12}
        onChange={changePhoneNumber}
        required
      />

      <Button className="blue regPhone" onClick={registerPhoneNumber}>
        Далее
      </Button>
    </Container>
  );
}

Login.propTypes = {
  registerPhoneNumber: PropTypes.func.isRequired,
  changePhoneNumber: PropTypes.func.isRequired,
  btnRegDisabled: PropTypes.bool.isRequired
};

export default Login;
