import React from 'react';
import { NavLink } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

import './Header.css';

const Header = () => {
  return (
    <Menu widths={3}>
      <NavLink to="/" className="item normal" activeClassName="active" exact>
        Забронировать
      </NavLink>

      <NavLink
        to="/schedules"
        className="item normal"
        activeClassName="active"
        exact
      >
        Рейсы
      </NavLink>

      <NavLink
        to="/orders"
        className="item normal"
        activeClassName="active"
        exact
      >
        История заказов
      </NavLink>
    </Menu>
  );
};

export default Header;
