import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Header from './Header';
import OrderCreate from '../passenger/OrderCreate';
import ScheduleList from '../passenger/ScheduleList';
import OrdersList from '../passenger/OrdersList';

class MainScreen extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Header />
        <Route path="/" exact component={OrderCreate}></Route>
        <Route path="/schedules" exact component={ScheduleList}></Route>
        <Route path="/orders" exact component={OrdersList}></Route>
      </BrowserRouter>
    );
  }
}

export default MainScreen;
