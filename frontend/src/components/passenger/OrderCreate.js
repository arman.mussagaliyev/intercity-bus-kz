import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Form, Header, Input, Dropdown } from 'semantic-ui-react';

import {
  fetchDepartures,
  selectDepAndFetchArrivals,
  selectDepDate,
  selectArrival
} from '../../actions';
import './OrderCreate.css';

class OrderCreate extends React.Component {
  departuresOptions = () => {
    return this.props.departures.list.map(depart => {
      return { key: depart.id, value: depart.id, text: depart.name };
    });
  };

  arrivalsOptions = () => {
    if (this.props.arrivals) {
      return this.props.arrivals.list.map(arrive => {
        return { key: arrive.id, value: arrive.id, text: arrive.name };
      });
    }
    return [];
  };

  componentDidMount() {
    this.props.fetchDepartures();
  }

  onChangeDepartureDate = e => {
    this.props.selectDepDate(e.target.value);
  };

  onChangeDeparture = (e, data) => {
    this.props.selectDepAndFetchArrivals(data.value);
  };

  onChangeArrival = (e, data) => {
    this.props.selectArrival(data.value);
  };

  render() {
    return (
      <Form>
        <Header as="h3">Забронировать поездку</Header>
        <Form.Field>
          <Input
            type="date"
            onChange={this.onChangeDepartureDate}
            value={this.props.depDate}
          ></Input>
        </Form.Field>

        <Form.Field>
          <label>Откуда: </label>
          <Dropdown
            placeholder="Станция отправления"
            clearable
            fluid
            search
            selection
            options={this.departuresOptions()}
            onChange={this.onChangeDeparture}
            value={this.props.depId}
          />
        </Form.Field>

        <Form.Field>
          <label>Куда: </label>
          <Dropdown
            placeholder="Станция прибытия"
            clearable
            fluid
            search
            selection
            value={this.props.arrivalId}
            options={this.arrivalsOptions()}
            onChange={this.onChangeArrival}
          />
        </Form.Field>

        {this.props.depId && this.props.arrivalId ? (
          <Form.Field>
            <NavLink to="/schedules" className="ui primary button">
              Выбрать рейс
            </NavLink>
          </Form.Field>
        ) : null}
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    departures: state.departures,
    arrivals: state.arrivals,
    depId: state.depId,
    arrivalId: state.arrivalId,
    depDate: state.depDate
  };
};

export default connect(mapStateToProps, {
  fetchDepartures,
  selectDepAndFetchArrivals,
  selectDepDate,
  selectArrival
})(OrderCreate);
