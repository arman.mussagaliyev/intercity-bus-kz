import React from 'react';
import { connect } from 'react-redux';
import { Item } from 'semantic-ui-react';

import './ScheduleList.css';
import { fetchSchedules } from '../../actions';

class ScheduleList extends React.Component {
  componentDidMount() {
    const { depDate, depId, arrivalId } = this.props;
    this.props.fetchSchedules(depDate, depId, arrivalId);
    /*
    setTimeout(() => {
      console.log('mount: ', this.props.schedules);
    }, 1000);
    */
  }

  render() {
    let scheduleList = '';
    const prepareSchedules = schedules => {
      //console.log(schedules);
      if (schedules.isFetching) {
        return schedules.errMessage;
      } else if (schedules.list.length === 0) {
        return 'Нет доступных рейсов';
      }
      const { list } = schedules;

      if (list[0].id || list[0].id === 0) {
        return list.map(data => {
          return (
            <Item key={data.id}>
              <Item.Content verticalAlign="middle">
                <Item.Meta>Описание рейса:</Item.Meta>
                <Item.Description>
                  <p>
                    Начало посадки: {data.departureTime} <br />
                    Время отправления: {data.boardingTime} <br />
                    Время прибытия: {data.arrivalTime}
                  </p>
                  <p>
                    Автобус: {data.fleet.brand} {data.fleet.model}, гос.номер:{' '}
                    {data.fleet.plateNumber}
                  </p>
                  <p>Водитель: {data.driver}</p>
                </Item.Description>
                <Item.Extra>Подробнее</Item.Extra>
              </Item.Content>
            </Item>
          );
        });
      }
      return '';
    };

    this.props.schedules.isError
      ? (scheduleList = this.props.schedules.errMessage.map((msg, i) => (
          <p key={i}>{msg}</p>
        )))
      : (scheduleList = prepareSchedules(this.props.schedules));

    return <Item.Group>{scheduleList}</Item.Group>;
  }
}

const mapStateToProps = state => {
  return {
    schedules: state.schedules,
    depDate: state.depDate,
    depId: state.depId,
    arrivalId: state.arrivalId
  };
};

export default connect(mapStateToProps, { fetchSchedules })(ScheduleList);
