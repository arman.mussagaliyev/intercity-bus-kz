import React from 'react';
import { Container } from 'semantic-ui-react';

import Login from './Login';
import MainScreen from './common/MainScreen';
import { savePhoneToLocalStore, getPhoneFromLS } from '../utils/utils';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      phone: null,
      showLogin: false,
      btnRegDisabled: true
    };
  }

  componentDidMount() {
    const phone = getPhoneFromLS();
    const showLogin = phone ? false : true;
    this.setState({ phone, showLogin });
  }

  registerPhoneNumber = () => {
    if (this.state.phone) {
      let showLogin = false;
      this.setState({ showLogin });
      savePhoneToLocalStore(this.state.phone);
    }
  };

  changePhoneNumber = evt => {
    let { phone, btnRegDisabled } = this.state;
    if (evt.target.validity.valid) {
      phone = evt.target.value;
      evt.target.classList.remove('invalid');
    }
    btnRegDisabled = !evt.target.validity.valid;

    this.setState({ phone, btnRegDisabled });
  };

  render() {
    return (
      <Container>
        {this.state.showLogin ? (
          <Login
            registerPhoneNumber={this.registerPhoneNumber}
            changePhoneNumber={this.changePhoneNumber}
            btnRegDisabled={this.state.btnRegDisabled}
          />
        ) : (
          <MainScreen />
        )}
      </Container>
    );
  }
}

export default App;
