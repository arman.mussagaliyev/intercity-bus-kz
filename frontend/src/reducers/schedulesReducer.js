import {
  FETCH_SCHEDULES,
  FETCH_SCHEDULES_SUCCESS,
  FETCH_SCHEDULES_ERROR
} from '../actions/types';

const initialState = {
  list: [],
  isFetching: false,
  isError: false,
  errMessage: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SCHEDULES:
      return {
        ...state,
        list: [],
        isFetching: true,
        isError: false,
        errMessage: action.payload
      };
    case FETCH_SCHEDULES_SUCCESS:
      return {
        ...state,
        list: action.payload,
        isFetching: false,
        isError: false,
        errMessage: ''
      };
    case FETCH_SCHEDULES_ERROR:
      return {
        ...state,
        isFetching: false,
        isError: true,
        errMessage: action.payload
      };
    default:
      return state;
  }
};
