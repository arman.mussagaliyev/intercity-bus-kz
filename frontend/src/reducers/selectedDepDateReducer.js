import { SELECT_DEP_DATE } from '../actions/types';
import { getCurrDateString } from '../utils/utils';

export default (state = getCurrDateString(), action) => {
  switch (action.type) {
    case SELECT_DEP_DATE:
      return action.payload;
    default:
      return state;
  }
};
