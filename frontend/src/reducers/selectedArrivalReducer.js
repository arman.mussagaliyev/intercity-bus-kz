import { SELECT_ARRIVAL } from '../actions/types';

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_ARRIVAL:
      return action.payload;
    default:
      return state;
  }
};
