import {
  FETCH_ARRIVALS,
  FETCH_ARRIVALS_SUCCESS,
  FETCH_ARRIVALS_ERROR
} from '../actions/types';

const initialState = {
  list: [{ id: null, name: '' }],
  isFetching: false,
  isError: false,
  errMessage: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARRIVALS:
      return {
        ...state,
        list: [{ id: -1, name: action.payload }],
        isFetching: true,
        isError: false,
        errMessage: ''
      };
    case FETCH_ARRIVALS_SUCCESS:
      return {
        ...state,
        list: action.payload,
        isFetching: false
      };
    case FETCH_ARRIVALS_ERROR:
      return { ...state, isError: true, errMessage: action.payload };
    default:
      return state;
  }
};
