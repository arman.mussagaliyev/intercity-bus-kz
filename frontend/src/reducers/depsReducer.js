import {
  FETCH_DEP,
  FETCH_DEP_SUCCESS,
  FETCH_DEP_ERROR
} from '../actions/types';

const initialState = {
  list: [{ id: null, name: '' }],
  isFetching: false, // изначально статус загрузки - ложь
  isError: false,
  errMessage: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DEP:
      return {
        ...state,
        list: [{ id: -1, name: action.payload }],
        isFetching: true,
        isError: false,
        errMessage: ''
      };
    case FETCH_DEP_SUCCESS:
      return {
        ...state,
        list: action.payload,
        isFetching: false
      };
    case FETCH_DEP_ERROR:
      return { ...state, isError: true, errMessage: action.payload };
    default:
      return state;
  }
};
