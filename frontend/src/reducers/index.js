import { combineReducers } from 'redux';
import departures from './depsReducer';
import arrivals from './arrivalsReducer';
import depId from './selectedDepReducer';
import arrivalId from './selectedArrivalReducer';
import depDate from './selectedDepDateReducer';
import schedules from './schedulesReducer';

export default combineReducers({
  departures,
  arrivals,
  depId,
  arrivalId,
  depDate,
  schedules
});
