import { SELECT_DEP } from '../actions/types';

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_DEP:
      return action.payload;
    default:
      return state;
  }
};
